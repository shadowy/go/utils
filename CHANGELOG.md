<a name="unreleased"></a>
## [Unreleased]


<a name="v1.0.2"></a>
## [v1.0.2] - 2019-07-25
### Features
- add help function for working with array


<a name="v1.0.1"></a>
## v1.0.1 - 2019-07-23
### Features
- add pointer function


[Unreleased]: https://gitlab.com/shadowy/go/utils/compare/v1.0.2...HEAD
[v1.0.2]: https://gitlab.com/shadowy/go/utils/compare/v1.0.1...v1.0.2
