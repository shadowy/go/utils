package utils

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestPtrString(t *testing.T) {
	assert.Equal(t, "1", *PtrString("1"))
}

func TestPtrInt(t *testing.T) {
	assert.Equal(t, 1, *PtrInt(1))
}

func TestPtrInt8(t *testing.T) {
	assert.Equal(t, int8(1), *PtrInt8(1))
}

func TestPtrInt16(t *testing.T) {
	assert.Equal(t, int16(1), *PtrInt16(1))
}

func TestPtrInt32(t *testing.T) {
	assert.Equal(t, int32(1), *PtrInt32(1))
}

func TestPtrInt64(t *testing.T) {
	assert.Equal(t, int64(1), *PtrInt64(1))
}

func TestPtrUInt(t *testing.T) {
	assert.Equal(t, uint(1), *PtrUInt(1))
}

func TestPtrUInt8(t *testing.T) {
	assert.Equal(t, uint8(1), *PtrUInt8(1))
}

func TestPtrUInt16(t *testing.T) {
	assert.Equal(t, uint16(1), *PtrUInt16(1))
}

func TestPtrUInt32(t *testing.T) {
	assert.Equal(t, uint32(1), *PtrUInt32(1))
}

func TestPtrUInt64(t *testing.T) {
	assert.Equal(t, uint64(1), *PtrUInt64(1))
}

func TestPtrBook(t *testing.T) {
	assert.Equal(t, true, *PtrBool(true))
}

func TestPtrFloat32(t *testing.T) {
	assert.Equal(t, float32(1), *PtrFloat32(1))
}

func TestPtrFloat64(t *testing.T) {
	assert.Equal(t, float64(1), *PtrFloat64(1))
}

func TestPtrTime(t *testing.T) {
	tm := time.Now()
	assert.Equal(t, tm, *PtrTime(tm))
}
