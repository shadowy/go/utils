package utils

import "time"

// PtrString - string to *string
func PtrString(value string) *string {
	return &value
}

// PtrInt - int to *int
func PtrInt(value int) *int {
	return &value
}

// PtrInt8 - int8 to *int8
func PtrInt8(value int8) *int8 {
	return &value
}

// PtrInt16 - int16 to *int16
func PtrInt16(value int16) *int16 {
	return &value
}

// PtrInt32 - int32 to *int32
func PtrInt32(value int32) *int32 {
	return &value
}

// PtrInt64 - int to *int64
func PtrInt64(value int64) *int64 {
	return &value
}

// PtrUInt - uint to *uint
func PtrUInt(value uint) *uint {
	return &value
}

// PtrUInt8 - uint8 to *uint8
func PtrUInt8(value uint8) *uint8 {
	return &value
}

// PtrUInt16 - uint16 to *uint16
func PtrUInt16(value uint16) *uint16 {
	return &value
}

// PtrUInt32 - uint32 to *uint32
func PtrUInt32(value uint32) *uint32 {
	return &value
}

// PtrUInt64 - uint64 to *uint64
func PtrUInt64(value uint64) *uint64 {
	return &value
}

// PtrBool - bool to *bool
func PtrBool(value bool) *bool {
	return &value
}

// PtrFloat32 - float32 to *float32
func PtrFloat32(value float32) *float32 {
	return &value
}

// PtrFloat64 - float64 to *float64
func PtrFloat64(value float64) *float64 {
	return &value
}

// PtrTime - time to *time
func PtrTime(value time.Time) *time.Time {
	return &value
}
